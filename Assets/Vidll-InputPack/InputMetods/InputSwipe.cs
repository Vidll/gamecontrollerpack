using UnityEngine;

public class InputSwipe : InputCustom
{
	[SerializeField] private InputSwipeState _currentSwipeState;

	private Vector3 _startTouchPos;
	private Vector3 _holdTouchPos;

	private float _differenceX = 0;
	private float _differenceY = 0;

	private bool _swiped;

	public float SwipeDistantion = 100f;

	public delegate void SwipeAction();
	public SwipeAction Swiped;
	public SwipeAction SwipedHorizontal;
	public SwipeAction SwipedVertical;
	public SwipeAction SwipedLeft;
	public SwipeAction SwipedRight;
	public SwipeAction SwipedUp;
	public SwipeAction SwipedDown;

	public override void InputInitInAwake()
	{
		base.InputInitInAwake();
	}

	public override void InputInitInUpdate()
	{
		base.InputInitInUpdate();
		if (Input.GetMouseButtonDown(0))
		{
			_startTouchPos = Input.mousePosition;
			InputStart();
			_swiped = true;
		}
		else if(Input.GetMouseButton(0))
		{
			if (_swiped)
			{
				_holdTouchPos = Input.mousePosition;
				switch (_currentSwipeState) 
				{
					case (InputSwipeState.HorizontalSwipe):
						_differenceX = _holdTouchPos.x - _startTouchPos.x;
 						break;
					case (InputSwipeState.VerticalSwipe):
						_differenceY = _holdTouchPos.y - _startTouchPos.y;
						break;
					case (InputSwipeState.VerticalAndHorizontalSwipe):
						_differenceX = _holdTouchPos.x - _startTouchPos.x;
						_differenceY = _holdTouchPos.y - _startTouchPos.y;
						break;
					case (InputSwipeState.None):
						Debug.LogError("Choose input swipe type on Vidll-InputPack\\Resources\\InputSwipe");
						break;
				}
				CheckSwipe();
			}
			InputUpdating();
		}
		else if (Input.GetMouseButtonUp(0))
		{
			_startTouchPos = Vector3.zero;
			_holdTouchPos = Vector3.zero;
			_differenceX = 0;
			_differenceY = 0;
			InputEnd();
		}
	}

	private void CheckSwipe()
	{
		if (_differenceX >= SwipeDistantion || _differenceX <= -SwipeDistantion)
		{
			_swiped = false;
			Swiped?.Invoke();
			SwipedHorizontal?.Invoke();
			if (_differenceX >= SwipeDistantion)
				SwipedRight?.Invoke();
			else
				SwipedLeft?.Invoke();
		}
		else if (_differenceY >= SwipeDistantion || _differenceY <= -SwipeDistantion)
		{
			_swiped = false;
			Swiped?.Invoke();
			SwipedVertical?.Invoke();
			if (_differenceY >= SwipeDistantion)
				SwipedUp?.Invoke();
			else
				SwipedDown?.Invoke();
		}
	}

	public override void InputStart()
	{
		base.InputStart();
	}

	public override void InputEnd()
	{
		base.InputEnd();
	}

	public override void InputUpdating()
	{
		base.InputUpdating();
	}

	public enum InputSwipeState
	{
		None = 0,
		VerticalSwipe,
		HorizontalSwipe,
		VerticalAndHorizontalSwipe
	}
}