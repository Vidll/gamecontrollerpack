using UnityEngine;

public class InputDeviceButtons : InputCustom
{
    [Header("Set button keys")]
	public KeyCode[] KeyCodes;

	public delegate void Click(KeyCode key);
	public Click DownButton;
	public Click UpButton;
	public Click HoldButton;

	public override void InputInitInAwake()
	{
		base.InputInitInAwake();
		if(KeyCodes.Length < 1)
			Debug.Log("Set KetCode on Vidll-InputPack\\Resources\\InputDeviceButtons");
	}

	public override void InputInitInUpdate()
	{
		base.InputInitInUpdate();
		foreach (var key in KeyCodes)
		{
			if (Input.GetKeyDown(key))
			{
				InputStart();
				DownButton?.Invoke(key);
			}
			else if (Input.GetKeyUp(key))
			{
				InputEnd();
				UpButton?.Invoke(key);
			}
			else if (Input.GetKey(key))
			{
				InputUpdating();
				HoldButton?.Invoke(key);
			}
		}
	}

	public void SetKeyCode(KeyCode key) => KeyCodes[KeyCodes.Length] = key;
	public override void InputStart() => base.InputStart();
	public override void InputEnd() => base.InputEnd();
	public override void InputUpdating() => base.InputUpdating();
}
