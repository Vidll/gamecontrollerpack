using UnityEngine;

public class InputMouseMoved : OutVector
{
	[Range(1, 100)] public float SensitivityX = 1;
	[Range(1, 100)] public float SensitivityY = 1;

	public float _valueTimeStopZone = 1f;

	private bool _movedX = false;
	private bool _movedY = false;
	private bool _isStopped = false;

	private float _timeDeadZoneX = 0f;
	private float _timeDeadZoneY = 0f;

	public override void InputInitInUpdate()
	{
		base.InputInitInUpdate();

		if (Input.GetAxisRaw("Mouse X") != 0)
		{
			if (!_movedX)
			{
				InputStart();
				_movedX = true;
			}
			InputUpdating();
			_outFloatX = Input.GetAxisRaw("Mouse X") * SensitivityX;
			_isStopped = false;
			_timeDeadZoneX = 0;
		}
		if (Input.GetAxisRaw("Mouse Y") != 0)
		{
			if (!_movedY)
			{
				InputStart();
				_movedY = true;
			}
			InputUpdating();
			_outFloatY = Input.GetAxisRaw("Mouse Y") * SensitivityY;
			_isStopped = false;
			_timeDeadZoneY = 0;
		}

		if (_movedX && _timeDeadZoneX >= _valueTimeStopZone)
			StopMovedX();
		else if (_movedY && _timeDeadZoneY >= _valueTimeStopZone)
			StopMovedY();
		else if (!_movedX && !_movedY && !_isStopped)
			InputEnd();

		_timeDeadZoneX += Time.deltaTime;
		_timeDeadZoneY += Time.deltaTime;
	}

	private void StopMovedX()
	{
		_movedX = false;
		_outFloatX = 0;
	}

	private void StopMovedY()
	{
		_movedY = false;
		_outFloatY = 0;
	}

	public override void InputStart()
	{
		if(!_movedX && !_movedY)
			base.InputStart();
	}

	public override void InputUpdating()
	{
		base.InputUpdating();
		_outVector3 = Input.mousePosition;
	}

	public override void InputEnd()
	{
		base.InputEnd();
		_isStopped = true;
	}
}
