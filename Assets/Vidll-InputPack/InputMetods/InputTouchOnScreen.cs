using UnityEngine;

public class InputTouchOnScreen : InputCustom
{
	public override void InputInitInUpdate()
	{
		base.InputInitInUpdate();
		if (Input.GetMouseButtonDown(0))
			InputStart();
		else if (Input.GetMouseButton(0))
			InputUpdating();
		if (Input.GetMouseButtonUp(0))
			InputEnd();
	}

	public override void InputStart() => base.InputStart();
	public override void InputEnd() => base.InputEnd();
	public override void InputUpdating() => base.InputUpdating();
}
