using UnityEngine;
using System.Collections.Generic;

public class InputClickObjectsMethod : InputCustom
{
	[HideInInspector] public List<GameObject> NeedClickedGameObjects;

	private int _objectsCount = 0;

	public MovedType CurrentMovedType;
	public float SpeedMovedObject = 2;

	public override void InputInitInAwake()
	{
		base.InputInitInAwake();
		if (NeedClickedGameObjects.Count > 0)
		{
			NeedClickedGameObjects.Clear();
			_objectsCount = 0;
		}
	}

	public override void InputInitInUpdate()
	{
		base.InputInitInUpdate();
	}

	public void AddClickedObject(GameObject obj, MovedType movedType, float YourSpeed = 0)
	{
		if (YourSpeed == 0)
			YourSpeed = SpeedMovedObject;

		InitGameObject(obj, YourSpeed, movedType);
		NeedClickedGameObjects.Add(obj);
	}

	public void AddClickedObjects(GameObject[] objs, MovedType movedType, float customSpeed = 0)
	{
		if (customSpeed == 0)
			customSpeed = SpeedMovedObject;

		foreach (var obj in objs)
		{
			InitGameObject(obj, customSpeed, movedType);
			NeedClickedGameObjects.Add(obj);
		}
	}

	public void ChangeObjectMovedType(int indexObject, MovedType newMovedType)
	{
		if (indexObject >= NeedClickedGameObjects.Count)
		{
			Debug.LogError("Index object error: dont have object " + indexObject + " index");
			return;
		}
		NeedClickedGameObjects[indexObject].GetComponent<TouchObjectForMethod>().MovedType = ((int)newMovedType);
	}

	public void ChangeObjectMovedSpeed(int indexObject, float newSpeed)
	{
		if (indexObject >= NeedClickedGameObjects.Count)
		{
			Debug.LogError("Index object error: dont have object " + indexObject + " index");
			return;
		}
		NeedClickedGameObjects[indexObject].GetComponent<TouchObjectForMethod>().MovedSpeed = newSpeed;
	}

	public void DontCheckAllObjects()
	{
		foreach (var obj in NeedClickedGameObjects)
		{
			if (obj)
			{
				var component = obj.GetComponent<TouchObjectForMethod>();
				if (component)
					Destroy(component);
			}
		}
		NeedClickedGameObjects.Clear();
		_objectsCount = 0;
	}

	public void DontCheckOneObject(int numberOnList)
	{
		var component = NeedClickedGameObjects[numberOnList].GetComponent<TouchObjectForMethod>();
		if (component)
			Destroy(component);
		NeedClickedGameObjects.RemoveAt(numberOnList);
	}

	private void InitGameObject(GameObject obj, float speed, MovedType movedType)
	{
		if (obj.TryGetComponent(out TouchObjectForMethod touchObjectOld))
			touchObjectOld.Init(this, speed, (int)movedType, _objectsCount);
		else
		{
			var touchObject = obj.AddComponent<TouchObjectForMethod>();
			touchObject.Init(this, speed, (int)movedType, _objectsCount);
		}
		_objectsCount++;
	}

	public override void InputStart() => base.InputStart();
	public override void InputUpdating() => base.InputUpdating();
	public override void InputEnd() => base.InputEnd();
	
	public enum MovedType
	{
		NONE = 0,
		XY,
		XZ,
		ZY,
	}
}