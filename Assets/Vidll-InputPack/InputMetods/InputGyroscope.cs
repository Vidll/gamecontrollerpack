using UnityEngine;

public class InputGyroscope : OutQuaternion
{
	private Gyroscope _gyroscope;
	private bool _gyroEnable;

	public override void InputInitInAwake()
	{
		base.InputInitInAwake();
		_gyroEnable = SystemInfo.supportsGyroscope;
		if (_gyroEnable)
		{
			_gyroscope = Input.gyro;
			EnableGyroscope();
		}
		else
			Debug.Log("Gyroscope dont supported");
	}

	public override void InputInitInUpdate()
	{
		base.InputInitInUpdate();
		if (!_gyroEnable)
			return;

		_outVector3 = _gyroscope.attitude.eulerAngles;
		_outQuaternion = _gyroscope.attitude;

		InputUpdating();
	}

	public override void InputUpdating()
	{
		base.InputUpdating();
	}

	public void EnableGyroscope() => _gyroscope.enabled = true;
	public void DisableGyroscope() => _gyroscope.enabled = false;
}
