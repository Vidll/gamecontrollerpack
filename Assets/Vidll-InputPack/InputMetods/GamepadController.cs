using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GamepadController : MonoBehaviour , IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	[SerializeField] private Image _gamepadBG;
	[SerializeField] private Image _stick;

	private Vector2 _inputVector;
	private Vector2 _pos;
	private Vector2 _startPosition;

	private bool _available = false;

	public delegate void UseInput();
	public UseInput StartInput;
	public UseInput EndInput;
	public UseInput UpdateInput;

	public bool FixedGamePad = true;
	public bool BackOnStartPosition = true;

	public void Init()
	{
		if (!_gamepadBG)
		{
			_gamepadBG = transform.GetChild(0).GetComponent<Image>();
			_stick = _gamepadBG.transform.GetChild(0).GetComponent<Image>();
		}
		InputEnable();
		print(this.name);

		_startPosition = _gamepadBG.transform.position;
	}

	public virtual void OnPointerDown(PointerEventData ped)
	{
		if (!_available)
			return;
		if (!FixedGamePad)
		{
			var pos = Input.mousePosition;
			_gamepadBG.transform.position = pos;
		}
		StartInput?.Invoke();
		OnDrag(ped);
	}
	
	public virtual void OnPointerUp(PointerEventData ped)
	{
		if (!_available)
			return;
		_inputVector = Vector2.zero;
		_stick.rectTransform.anchoredPosition = Vector2.zero;

		if(BackOnStartPosition)
			_gamepadBG.transform.position = _startPosition;

		EndInput?.Invoke();
	}

	public virtual void OnDrag(PointerEventData ped)
	{
		if (!_available)
			return;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_gamepadBG.rectTransform, ped.position, ped.pressEventCamera, out _pos))
		{
			UpdateInput?.Invoke();
			_pos.x = (_pos.x / _gamepadBG.rectTransform.sizeDelta.x);
			_pos.y = (_pos.y / _gamepadBG.rectTransform.sizeDelta.x);

			_inputVector = new Vector2(_pos.x * 2, _pos.y * 2);
			_inputVector = (_inputVector.magnitude > 1.0f) ? _inputVector.normalized : _inputVector;

			_stick.rectTransform.anchoredPosition = new Vector2(_inputVector.x * (_gamepadBG.rectTransform.sizeDelta.x / 2), _inputVector.y * (_gamepadBG.rectTransform.sizeDelta.y / 2));
		}
	}

	public float GetHorizontalFloat()
	{
		if (!_available)
			return 0;
		else if (_inputVector.x != 0)
			return _inputVector.x;
		else 
			return Input.GetAxis("Horizontal");
	}

	public float GetVerticalFloat()
	{
		if (!_available)
			return 0;
		else if (_inputVector.y != 0)
			return _inputVector.y;
		else 
			return Input.GetAxis("Vertical");
	}

	public Vector2 GetVector()
	{
		if (!_available)
			return Vector2.zero;
		else if (_inputVector != Vector2.zero)
			return _inputVector;
		else
			return Vector2.zero;
	}

	public virtual void InputEnable() => _available = true;
	public virtual void InputDisable() => _available = false;
}
