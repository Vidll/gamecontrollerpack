﻿using UnityEngine;
using System.Collections.Generic;

public class InputClickObjectsRaycast : InputCustom
{
    [SerializeField] private LayerMask _layerTouchZone;
    [SerializeField] private LayerMask _layerTouchObject;

    [SerializeField] private bool _lookRayLine = false;
    [SerializeField] private bool _zoneHit = true;

    [HideInInspector] public List<GameObject> NeedClickedGameObjects;

    private Camera _outRayCamera;
    private TouchObjectForRaycast _currentObject;

    private int _objectsCount = 0;

    public float SpeedMovedObject = 2;
    public float RayLength = 1000f;

    public DirectionType CurrentMovedType;
    public DirectionType CurrentSetValueType;

    public Camera OutRayCamera { get => _outRayCamera; set { _outRayCamera = value; } }

	public override void InputInitInAwake()
	{
		base.InputInitInAwake();

        if (NeedClickedGameObjects.Count > 0)
		{
            NeedClickedGameObjects.Clear();
            _objectsCount = 0;
		}

        if (!_outRayCamera)
            _outRayCamera = Camera.main;
    }

	public override void InputInitInUpdate()
	{
		base.InputInitInUpdate();
        if (Input.GetMouseButtonDown(0))
            MouseButtonDown();
        else if (Input.GetMouseButton(0) && _currentObject)
		{
            _currentObject.MouseDrag(GetTouchPosition());
            InputUpdating();
        }
        else if (Input.GetMouseButtonUp(0) && _currentObject)
		{
            _currentObject.MouseUp();
            _currentObject = null;
            CurrentTouchObject = null;
            InputEnd();
        }
	}

    private void MouseButtonDown()
    {
        RaycastHit hit;
        Ray ray = _outRayCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, RayLength, _layerTouchObject))
        {
            if (hit.collider.gameObject.TryGetComponent(out TouchObjectForRaycast touchObject))
            {
                _currentObject = touchObject;
                CurrentTouchObject = touchObject.gameObject;
                _currentObject.MouseDown();
                Debug.Log("Touch object: " + _currentObject);
                InputStart();
            }
        }
    }

    private Vector3 GetTouchPosition()
    {
        RaycastHit hit;
        Ray ray = _outRayCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, RayLength, _zoneHit ? _layerTouchZone : _layerTouchObject))
        {
            if (_lookRayLine)
			{
                Debug.DrawLine(ray.origin, hit.point);
                Debug.Log("Vector point:" + hit.point);
            }
            return hit.point;
        }
        return _currentObject.transform.localPosition;
    }

    private void InitClickedObject(GameObject obj, DirectionType movedType, DirectionType setValueType, float speed)
    {
        if (obj.TryGetComponent(out TouchObjectForRaycast touchObjectOld))
		{
            touchObjectOld.Init(this, speed, (int)movedType, (int)setValueType, _objectsCount);
            touchObjectOld.gameObject.layer = GetLayerIndex(_layerTouchObject.value);
        }
        else
        {
            var touchObject = obj.AddComponent<TouchObjectForRaycast>();
            touchObject.Init(this, speed, (int)movedType, (int)setValueType, _objectsCount);
            touchObject.gameObject.layer = GetLayerIndex(_layerTouchObject.value);
        }
        _objectsCount++;
    }

    public void DontCheckAllObjects()
	{
        foreach(var obj in NeedClickedGameObjects) 
        {
            var component = obj.GetComponent<TouchObjectForRaycast>();
            if(component)
                Destroy(component);
        }
        NeedClickedGameObjects.Clear();
        _objectsCount = 0;
	}

    public void DontCheckOneObject(int numberOnList)
	{
        var component = NeedClickedGameObjects[numberOnList].GetComponent<TouchObjectForRaycast>();
        if (component)
            Destroy(component);
        NeedClickedGameObjects.RemoveAt(numberOnList);
	}

    public void AddClickedObject(GameObject clickObject, DirectionType movedType, DirectionType setValueType, float movedSpeed = 0)
	{
        if (movedSpeed == 0)
            movedSpeed = SpeedMovedObject;

        InitClickedObject(clickObject, movedType, setValueType,movedSpeed);
        NeedClickedGameObjects.Add(clickObject);
	}

    public void AddClickedObjects(GameObject[] clickObjects, DirectionType movedType, DirectionType setValueType, float movedSpeed = 0)
	{
        if (movedSpeed == 0)
            movedSpeed = SpeedMovedObject;

        foreach(var obj in clickObjects)
		{
            InitClickedObject(obj, movedType, setValueType, movedSpeed);
            NeedClickedGameObjects.Add(obj);
		}
	}

    public void ChangeObjectMovedSpeed(int indexObject, float newSpeed)
	{
        if(indexObject >= NeedClickedGameObjects.Count)
		{
            Debug.LogError("Index object error: dont have object " + indexObject + " index");
            return;
		}
        NeedClickedGameObjects[indexObject].
            GetComponent<TouchObjectForRaycast>().MovedSpeed = newSpeed;
	}

    public void ChangeObjectMovedType(int indexObject, DirectionType newMovedType)
	{
        if (indexObject >= NeedClickedGameObjects.Count)
        {
            Debug.LogError("Index object error: dont have object " + indexObject + " index");
            return;
        }
        NeedClickedGameObjects[indexObject].
            GetComponent<TouchObjectForRaycast>().MovedType = (int)newMovedType;
    }

    public void ChangeObjectSetValueType(int indexObject, DirectionType newSetValueType)
    {
        if (indexObject >= NeedClickedGameObjects.Count)
        {
            Debug.LogError("Index object error: dont have object " + indexObject + " index");
            return;
        }
        NeedClickedGameObjects[indexObject].
            GetComponent<TouchObjectForRaycast>().SetValuesType = (int)newSetValueType;
    }

    private int GetLayerIndex(int value)
	{
        int i = 0;
		while (value > 1)
		{
            value = value / 2;
            i++;
		}
        return i;
	}

	public override void InputStart() => base.InputStart();
	public override void InputUpdating() => base.InputUpdating();
	public override void InputEnd() => base.InputEnd();

    public enum DirectionType
    {
        NONE = 0,
        XY,
        XZ,
        ZY,
    }
}