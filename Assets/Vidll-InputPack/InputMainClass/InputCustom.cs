using UnityEngine;

public class InputCustom : ScriptableObject
{
	private GlobalInput.InputMethods _thisInputMethod;
	private bool _available = false;

	protected GameObject _currentTouchObject;

	public delegate void UseInput();
	public UseInput StartInput;
	public UseInput EndInput;
	public UseInput UpdateInput;

	public GameObject CurrentTouchObject { get => _currentTouchObject; set { _currentTouchObject = value; } }
	public GlobalInput.InputMethods NameInputMethod { get => _thisInputMethod; set { _thisInputMethod = value; } }

	public virtual void InputInitInAwake()
	{
		if (!_available)
			return;
	}

	public virtual void InputInitInUpdate()
	{
		if (!_available)
			return;
	}

	public virtual void InputStart()
	{
		if (!_available)
			return;
		StartInput?.Invoke();
	}

	public virtual void InputEnd()
	{
		if (!_available)
			return;
		EndInput?.Invoke();
	}

	public virtual void InputUpdating()
	{
		if (!_available)
			return;
		UpdateInput?.Invoke();
	}

	public virtual void InputEnable() => _available = true;
	public virtual void InputDisable() => _available = false;

	public InputCustom GetClass(InputCustom inputClass) =>
		inputClass.NameInputMethod == NameInputMethod ? this : null;
	public InputCustom GetClass(GlobalInput.InputMethods name) =>
		name == NameInputMethod ? this : null;
}