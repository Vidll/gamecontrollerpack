using UnityEngine;

public abstract class OutVector : OutMyFloat
{
    protected Vector3 _outVector3;
    public Vector3 OutMyVector { get => _outVector3; }
}
