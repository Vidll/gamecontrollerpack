using UnityEngine;

public abstract class OutMyFloat : InputCustom
{
    protected float _outFloatX;
    protected float _outFloatY;

    public float OutFloatX { get => _outFloatX; }
    public float OutFloatY { get => _outFloatY; }
}
