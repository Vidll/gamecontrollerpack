using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputSceneController : MonoBehaviour
{
	public GlobalInput.InputMethods[] UsedInputMethods;

	[Space][Header("Set the game objects that you need to click after the start")]
	[Header("If use InputClickObjectsMethod, InputClickObjectsRaycast")]
	public GameObject[] InputClickGameObjects;

	private List<UnityAction> _actions = new List<UnityAction>();
	private GamepadController _gamepadController;

	private void Awake()
	{
		SetMethods();
	}

	private void Update()
	{
		foreach (var action in _actions)
			action();
	}

    private void SetMethods()
	{
		foreach (var input in UsedInputMethods)
		{
			switch (input)
			{
				case GlobalInput.InputMethods.InputDeviceButtons:				//Device buttons
					InputDeviceButtons deviceObj = Resources.Load<InputDeviceButtons>(nameof(GlobalInput.InputMethods.InputDeviceButtons));
					deviceObj.NameInputMethod = GlobalInput.InputMethods.InputDeviceButtons;
					GlobalInput.InputDeviceButtons = deviceObj;
					EnableInputObject(deviceObj.GetClass(GlobalInput.InputMethods.InputDeviceButtons));
					break;
				case GlobalInput.InputMethods.InputMouseMoved:					//Mouse moved
					InputMouseMoved mouseObj = Resources.Load<InputMouseMoved>(nameof(GlobalInput.InputMethods.InputMouseMoved));
					mouseObj.NameInputMethod = GlobalInput.InputMethods.InputMouseMoved;
					GlobalInput.InputMouseMoved = mouseObj;
					EnableInputObject(mouseObj.GetClass(GlobalInput.InputMethods.InputMouseMoved));
					break;
				case GlobalInput.InputMethods.InputTouchOnScreen:               //Touch on screen
					InputTouchOnScreen touchOnScreenObj = Resources.Load<InputTouchOnScreen>(nameof(GlobalInput.InputMethods.InputTouchOnScreen));
					touchOnScreenObj.NameInputMethod = GlobalInput.InputMethods.InputTouchOnScreen;
					GlobalInput.InputTouchOnScreen = touchOnScreenObj;
					EnableInputObject(touchOnScreenObj.GetClass(GlobalInput.InputMethods.InputTouchOnScreen));
					break;
				case GlobalInput.InputMethods.InputSwipe:						//Swipe
					InputSwipe inputSwipe = Resources.Load<InputSwipe>(nameof(GlobalInput.InputMethods.InputSwipe));
					inputSwipe.NameInputMethod = GlobalInput.InputMethods.InputSwipe;
					GlobalInput.InputSwipe = inputSwipe;
					EnableInputObject(inputSwipe.GetClass(GlobalInput.InputMethods.InputSwipe));
					break;
				case GlobalInput.InputMethods.Gamepad:							//Gamepad
					var gamepadControllers = Resources.FindObjectsOfTypeAll<GamepadController>();
					if (gamepadControllers.Length > 0)
					{
						foreach (var controller in gamepadControllers)
							controller.gameObject.SetActive(true);

						GlobalInput.GamepadController = FindObjectsOfType<GamepadController>();
						foreach (var controller in GlobalInput.GamepadController)
							controller.Init();
					}
					else
						Debug.Log("There is not a single gamepad on the scene");
					break;
				case GlobalInput.InputMethods.InputGyroscope:                   //Gyroscope
					InputGyroscope inputGyroscope = Resources.Load<InputGyroscope>(nameof(GlobalInput.InputMethods.InputGyroscope));
					inputGyroscope.NameInputMethod = GlobalInput.InputMethods.InputGyroscope;
					GlobalInput.InputGyroscope = inputGyroscope;
					EnableInputObject(inputGyroscope.GetClass(GlobalInput.InputMethods.InputGyroscope));
					break;
				case GlobalInput.InputMethods.InputClickObjectsMethod:			//ClickObjects Method
					InputClickObjectsMethod inputClickObjectsMethod = Resources.Load<InputClickObjectsMethod>(nameof(GlobalInput.InputMethods.InputClickObjectsMethod));
					inputClickObjectsMethod.NameInputMethod = GlobalInput.InputMethods.InputClickObjectsMethod;
					GlobalInput.InputClickObjectsMethod = inputClickObjectsMethod;
					EnableInputObject(inputClickObjectsMethod.GetClass(GlobalInput.InputMethods.InputClickObjectsMethod));
					inputClickObjectsMethod.AddClickedObjects(InputClickGameObjects, inputClickObjectsMethod.CurrentMovedType);
					break;
				case GlobalInput.InputMethods.InputClickObjectsRaycast:         //ClickObjects Raycast
					InputClickObjectsRaycast inputClickObjectsRaycast = Resources.Load<InputClickObjectsRaycast>(nameof(GlobalInput.InputMethods.InputClickObjectsRaycast));
					inputClickObjectsRaycast.NameInputMethod = GlobalInput.InputMethods.InputClickObjectsRaycast;
					GlobalInput.InputClickObjectsRaycast = inputClickObjectsRaycast;
					EnableInputObject(inputClickObjectsRaycast.GetClass(GlobalInput.InputMethods.InputClickObjectsRaycast));
					inputClickObjectsRaycast.AddClickedObjects(InputClickGameObjects, inputClickObjectsRaycast.CurrentMovedType, inputClickObjectsRaycast.CurrentSetValueType);
					break;
			}
		}
	}

	private void EnableInputObject(InputCustom input)
	{
		if (!input)
			return;
		print(input);
		input.InputEnable();
		input.InputInitInAwake();
		_actions.Add(input.InputInitInUpdate);
	}
}