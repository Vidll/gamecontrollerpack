
using UnityEngine;

public class OutQuaternion : OutVector
{
    protected Quaternion _outQuaternion;
    public Quaternion OutMyQuaternion { get => _outQuaternion; }
}
