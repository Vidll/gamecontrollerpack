public static class GlobalInput
{
	private static InputDeviceButtons _inputDeviceButtons;
	private static InputMouseMoved _inputMouseMoved;
	private static InputTouchOnScreen _inputTouchOnScreen;
	private static InputSwipe _inputSwipe;
	private static GamepadController[] _gamepadController;
	private static InputGyroscope _inputGyroscope;
	private static InputClickObjectsMethod _inputClickObjectsMethod;
	private static InputClickObjectsRaycast _inputClickObjectsRaycast;

	public static InputDeviceButtons InputDeviceButtons { get => _inputDeviceButtons; set { _inputDeviceButtons = value; } }
	public static InputMouseMoved InputMouseMoved { get => _inputMouseMoved; set { _inputMouseMoved = value; } }
	public static InputTouchOnScreen InputTouchOnScreen { get => _inputTouchOnScreen; set { _inputTouchOnScreen = value; } }
	public static InputSwipe InputSwipe { get => _inputSwipe; set { _inputSwipe = value; } }
	public static GamepadController[] GamepadController { get => _gamepadController; set { _gamepadController = value; } }
	public static InputGyroscope InputGyroscope { get => _inputGyroscope; set { _inputGyroscope = value; } }
	public static InputClickObjectsMethod InputClickObjectsMethod { get => _inputClickObjectsMethod; set { _inputClickObjectsMethod = value; } }
	public static InputClickObjectsRaycast InputClickObjectsRaycast { get => _inputClickObjectsRaycast; set { _inputClickObjectsRaycast = value; } }

	public enum InputMethods
	{
		None = 0,
		InputDeviceButtons,
		InputMouseMoved,
		InputTouchOnScreen,
		InputSwipe,
		Gamepad,
		InputGyroscope,
		InputClickObjectsMethod,
		InputClickObjectsRaycast,
	}
}