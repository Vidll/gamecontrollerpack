﻿using UnityEngine;

public class TouchObjectForRaycast : TouchObject
{
    [SerializeField] protected int _setValuesType = 0;

    private Vector3 PointVector;

    public int SetValuesType { get => _setValuesType; set { _setValuesType = value; } }

    public void MouseDown() { }
    public void MouseUp() { }

    public void Init(InputCustom inputCustom, float speed, int movedType, int setValueType, int index)
	{
        _movedType = movedType;
        _movedSpeed = speed;
        _inputParent = inputCustom;
        _myTransform = GetComponent<Transform>();
        _setValuesType = setValueType;
        _index = index;
    }

    public void MouseDrag(Vector3 MouseVector)
    {
        PointVector = MouseVector;
        _myTransform.localPosition = Vector3.Lerp(
            _myTransform.localPosition,
            GetMovedPosition(PointVector),
            _movedSpeed * Time.deltaTime);
    }

    private Vector3 GetMovedPosition(Vector3 MouseVector)
	{
        Vector3 nextVector;
        Vector2 newMouseVector;

		switch (_setValuesType) 
        {
            case 0:
                Debug.Log("The type set values is NONE or not selected");
                newMouseVector = _myTransform.position;
                break;
            case 1:
                newMouseVector = new Vector2(MouseVector.x, MouseVector.y);
                break;
            case 2:
                newMouseVector = new Vector2(MouseVector.x, MouseVector.z);
                break;
            case 3:
                newMouseVector = new Vector2(MouseVector.z, MouseVector.y);
                break;
            default:
                goto case 0;
        }

		switch (_movedType)
		{
            case 0:
                Debug.Log("The type of movement is not selected");
                nextVector = _myTransform.position;
                break;
            case 1:
                nextVector = new Vector3(newMouseVector.x, newMouseVector.y, _myTransform.position.z);
                break;
            case 2:
                nextVector = new Vector3(newMouseVector.x, _myTransform.position.y , newMouseVector.y);
                break;
            case 3:
                nextVector = new Vector3(_myTransform.position.x , newMouseVector.y, newMouseVector.x);
                break;
            default:
                goto case 0;
        }
        return nextVector;
	}
}
