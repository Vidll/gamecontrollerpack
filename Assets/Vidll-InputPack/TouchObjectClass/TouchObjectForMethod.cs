using UnityEngine;

public class TouchObjectForMethod : TouchObject
{
	private Vector3 _nextPosition;
	private Vector3 _mousePosition;
	private Vector3 _mousePositionOld;

	private void OnMouseDown()
	{
		_inputParent.InputStart();
		_inputParent.CurrentTouchObject = gameObject;
		StartTouch();
	}

	private void OnMouseDrag()
	{
		_inputParent.InputUpdating();
		DragTouch();
	}

	private void OnMouseUp()
	{
		_inputParent.InputEnd();
		_inputParent.CurrentTouchObject = null;
		EndTouch();
	}

	private void StartTouch()
	{
		_mousePosition = Input.mousePosition;
		_mousePositionOld = Input.mousePosition;
	}

	private void DragTouch()
	{
		_mousePosition = Input.mousePosition;
		if (_mousePosition != _mousePositionOld)
			Moved();
		_mousePositionOld = Input.mousePosition;
	}

	private void EndTouch()
	{
		_mousePosition = Vector3.zero;
		_mousePositionOld = Vector3.zero;
		_nextPosition = Vector3.zero;
	}

	private void Moved()
	{
		switch (_movedType) 
		{
			case 0:
				Debug.Log("The type of movement is not selected");
				return;
			case 1:
				_nextPosition = new Vector3(_mousePosition.x - _mousePositionOld.x, _mousePosition.y - _mousePositionOld.y, 0);
				break;
			case 2:
				_nextPosition = new Vector3(_mousePosition.x - _mousePositionOld.x, 0 , _mousePosition.y - _mousePositionOld.y);
				break;
			case 3:
				_nextPosition = new Vector3(0, _mousePosition.y - _mousePositionOld.y, _mousePosition.x - _mousePositionOld.x);
				break;
		}
		_myTransform.Translate(_nextPosition * (_movedSpeed / 10) * Time.deltaTime);
	}

	public override void Init(InputCustom inputCustom, float speed, int movedType, int Index)
	{
		base.Init(inputCustom, speed, movedType, Index);
	}
}