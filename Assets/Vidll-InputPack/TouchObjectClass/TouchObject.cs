using UnityEngine;

public class TouchObject : MonoBehaviour
{
	[SerializeField] protected int _index = 0;
	[SerializeField] protected int _movedType = 0;
	[SerializeField] protected float _movedSpeed;

	protected Transform _myTransform;
	protected InputCustom _inputParent;

	public int MovedType { get => _movedType; set { _movedType = value; } }
	public float MovedSpeed { get => _movedSpeed; set { _movedSpeed = value; } }
	public int Index { get => _index; set { _index = value; } }

	public virtual void Init(InputCustom inputCustom, float speed, int movedType, int index)
	{
		_movedType = movedType;
		_movedSpeed = speed;
		_inputParent = inputCustom;
		_myTransform = GetComponent<Transform>();
		_index = index; 
	}
}